/**
* @File Name:   NF_AccountTriggerHandler.cls
* @Description:   
* @Author:      Recruiter
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2018-07-25  Sami Islam  Created the file/class -  Create a corresponding “contact” that will have the same name as the created “account”.
*/
public with sharing class NF_AccountTriggerHandler extends NF_AbstractTriggerHandler {
    public override void beforeUpdate(){

    }

    public override void afterUpdate(){

    }

    public override void beforeInsert(){

    }

    public override void afterInsert(){
      createContactFromAccount(trigger.new);
    }

    public override void afterDelete(){

    }

    public override void andFinally(){

    }
    
    /***************************************************************************
      Method to create the corresponding new “contact” record that will have the same name as the created “account”.
    ***************************************************************************/
    public void createContactFromAccount(list<Account> newAccLst){
      
      list<Contact> contLstToInsert = new list<Contact>();
      
      for(Account acc : newAccLst){ 
        contLstToInsert.add(new Contact(LastName = acc.Name, AccountId = acc.Id));
      }
      
      //insert Contact
      if(!contLstToInsert.isEmpty()){
        insert contLstToInsert;
      }
      
    }
}

                      